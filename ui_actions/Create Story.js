createStory();

function createStory() {
	
	// (1) Copy defect fields into a new story
	var date = new GlideDateTime();
	current.start_date = date;
	current.end_date = date.addDays(1);
	current.update();
	var story = new GlideRecord("rm_story");
	story.initialize();
	story.priority = current.priority;
	story.short_description = current.short_description;
	story.assignment_group = current.assignment_group;
	story.company = current.company;
	story.u_requested_by = current.u_requested_by;
	story.assigned_to = current.assigned_to;
	story.description = current.description;
	story.work_notes = fixjournal(current.work_notes.getJournalEntry(1));
	story.comments = fixjournal(current.comments.getJournalEntry(1)) ;
	story.type="Development";
	story.opened = current.opened;
	story.opened_by = current.opened_by;
	story.cmdb_ci = current.cmdb_ci;
	story.product = current.u_product;
	story.enhancement = current.sys_id;
	story.state = getNewState(current);
	story.classification = "Feature";
	/* adding new fields per initial list from Bojan --- JC */
	story.theme = current.u_theme_enh;
	story.u_charge_code = current.u_charge_code;
	story.epic = current.u_epic_enh;
	story.order = current.order;	
	/* end of add --- JC */
	var sysid = story.insert();
	GlideSysAttachment.copy('rm_enhancement', current.sys_id, 'rm_story', sysid );
	// (2) Redirect webpage to the new story (Ensure story displayed in scrum view)
	//gs.addInfoMessage(gs.getMessage("Story {0} created", story.number));
	
	action.setRedirectURL(story);
	//var redirectURL = action.getRedirectURL();
	//  redirectURL = redirectURL.replace("sysparm_view=", "sysparm_view=scrum");
	//action.setRedirectURL(redirectURL);
	//action.setReturnURL(current);
}

// journal entries set using getJournalEntry() contain name and time stamp
// fixjournal remove the extra content
function fixjournal(journalEntry){
	var regex= new RegExp('\n'); // searching for the first line break
	var journalEntry2=journalEntry;
	var i = journalEntry.search(regex);
	if (i>0) journalEntry2 = journalEntry.substring(i+1, journalEntry.length);
		return journalEntry2;
}

function getNewState(current) {
	if (current.state == -5)  return -6;		// draft -> draft
		if (current.state == 2)   return 2;		// work in progress -> work in progress
		if (current.state == 4)   return -8;		// testing/qa -> testing
		if (current.state == 11)  return -5;          // on hold -> blocked
		if (current.state == 3)   return 3;		// closed complete -> complete
		if (current.state == 7)   return 4;		// cancelled -> cancelled
		if (current.state == -4)  return -6;          // scoping -> draft
		if (current.state == -3)  return 1;		// awaiting approval -> ready
		if (current.state == 10)  return 3;		// deploy/launch -> complete
		return current.state;
}
