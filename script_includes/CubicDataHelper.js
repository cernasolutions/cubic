var CubicDataHelper = Class.create();
CubicDataHelper.prototype = {
	initialize: function() {
	},
	
	cloneRelease : function (oldSysID,newSysID) {
        gs.log('oldSysID: ' + oldSysID, "JDC");
        gs.log('newSysID: ' + newSysID, "JDC");
		
		var prodRel = new GlideRecord('m2m_product_release');
		prodRel.addQuery('release',oldSysID);
		prodRel.query();
		
		while (prodRel.next()){
			gs.log('oldReleaseProductFound',"JDC");
			prodRel.release= newSysID;
			var npr = prodRel.insert();
			gs.log(npr);
		}
		
		var teamRel = new GlideRecord('scrum_pp_team');
		gs.log('oldReleaseTeamFound',"JDC");
		teamRel.addQuery('release',oldSysID);
		teamRel.query();
		
		while (teamRel.next()){
			teamRel.release = newSysID;
			teamRel.insert();
		}
		
	},
	
	type: 'CubicDataHelper'
};